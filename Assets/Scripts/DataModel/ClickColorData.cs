﻿using UnityEngine;

namespace Scripts.DataModel
{
    [System.Serializable]
    public struct ClickColorData
    {
        [SerializeField] private int minClicksCounts;
        [SerializeField] private int maxClicksCounts;
        [SerializeField] private Color color;

        public int MinClicksCounts => minClicksCounts;
        public int MaxClicksCounts => maxClicksCounts;
        public Color Color => color;

        public bool IsInRange(int number)
        {
            return number >= MinClicksCounts && number <= MaxClicksCounts;
        }

        private void OnValidate()
        {
            if (minClicksCounts < 1)
            {
                minClicksCounts = 1;
            }

            if (maxClicksCounts <= minClicksCounts)
            {
                maxClicksCounts = minClicksCounts + 1;
            }
        }
    }
}