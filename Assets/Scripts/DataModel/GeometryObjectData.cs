﻿using System.Linq;
using UnityEngine;

namespace Scripts.DataModel
{ 
    [CreateAssetMenu(fileName = "GeometryObjectData", menuName = "GeometryObjectData", order = 1)]
    public class GeometryObjectData : ScriptableObject
    {
        [SerializeField] private ClickColorData[] clickColorData;

        public ClickColorData[] ClickColorData => clickColorData;

        public Color GetColor(int clickAmount)
        {
            for (int i = 0; i < clickColorData.Length; i++)
            {
                if (clickColorData[i].IsInRange(clickAmount))
                {
                    return clickColorData[i].Color;
                }
            }

            if(clickColorData.Length > 0)
            { 
                return clickColorData.Last().Color;
            }
            else
            {
                return Color.white;
            }
        }
    }
}
