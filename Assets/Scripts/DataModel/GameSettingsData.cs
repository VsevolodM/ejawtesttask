﻿using UnityEngine;

namespace Scripts.DataModel
{
    [CreateAssetMenu(fileName = "GameSettingsData", menuName = "GameSettingsData", order = 1)]
    public class GameSettingsData : ScriptableObject
    {
        private const float EDITOR_MIN_VALUE = 0.001f;

        [SerializeField] private float timeToAutoRecolor = 1f;

        public float TimeToAutoRecolor => timeToAutoRecolor;

        private void OnValidate()
        {
            if (timeToAutoRecolor <= EDITOR_MIN_VALUE)
            {
                timeToAutoRecolor = EDITOR_MIN_VALUE;
            }
        }
    }
}

