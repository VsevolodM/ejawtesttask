﻿using Scripts.Extensions;
using System;
using Scripts.Core.Types;
using UnityEngine;

namespace Scripts.Components
{
    public class Raycaster : Singleton<Raycaster>
    {
        public event Action<Vector3, GameObject> OnGameObjectRaycastedEvent;

        private RaycastHit hit;
        private Camera camera;
        private Ray ray;

        protected void Awake()
        {
            camera = Camera.main;
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                ray = camera.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit))
                {
                    OnGameObjectRaycastedEvent.SafeInvoke(hit.point, hit.collider.gameObject);
                }
            }
        }
    }
}
