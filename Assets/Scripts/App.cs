﻿using Scripts.Extensions;
using Scripts.Models;
using Scripts.Services;
using System;

namespace Scripts
{
    public static class App
    {
        public static event Action ApplicationStartEvent;

        public static AppModel Model { get; }
        public static AppServices Services { get; }

        static App()
        {
            Model = new AppModel();
            Services = new AppServices();
        }

        public static void Start()
        {
            ApplicationStartEvent.SafeInvoke();
        }
    }
}
