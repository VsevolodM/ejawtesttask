﻿using Scripts.Components;
using System.Collections.Generic;
using System.IO;
using Scripts.Models;
using Scripts.Views;
using UnityEngine;

namespace Scripts.Controllers
{
    public class SceneController : MonoBehaviour
    {
        private const string SPAWN_PLANE_TAG = "SpawnPlane";
        private string geometryBundlesPath = "AssetBundles/Geometry";

        private Raycaster raycaster;

        private List<GeometryObjectModel> geometryObjectModels;
        private GeometryObjectView[] loadedGeometryObjectViews;
        private List<GeometryObjectView> spawnedGeometryObjectViews;

        private void Awake()
        {
            geometryBundlesPath = Path.Combine(Application.dataPath, geometryBundlesPath);

            geometryObjectModels = new List<GeometryObjectModel>();
            spawnedGeometryObjectViews = new List<GeometryObjectView>();

            App.Services.AssetBundlesService.
                ReadAllBundlesFromDirectory<GeometryObjectView>(geometryBundlesPath, App.Model.GeometryDataModel.GeometryItemsNames, OnGeometryLoaded);

            raycaster = Raycaster.Instance;
            raycaster.OnGameObjectRaycastedEvent += OnGameObjectRaycasted;
        }

        private void OnGeometryLoaded(GeometryObjectView[] items)
        {
            loadedGeometryObjectViews = items;
        }

        private void OnGameObjectRaycasted(Vector3 position, GameObject raycastedGameObject)
        {
            if (raycastedGameObject.tag == SPAWN_PLANE_TAG)
            {
                SpawnGeometryView(position);
            }
            else
            {
                GeometryObjectView view = raycastedGameObject.GetComponent<GeometryObjectView>();

                if (view != null)
                {
                    view.OnClick();
                }
            }
        }

        private void SpawnGeometryView(Vector3 position)
        {
            GeometryObjectView view = loadedGeometryObjectViews.GetRandom();
            view = Instantiate(view, position, Quaternion.identity, transform);

            string geometryType = view.gameObject.GetUnclonedName();
            GeometryObjectModel model = GeometryObjectModelFactory.Create(geometryType);
            geometryObjectModels.Add(model);

            view.Initialize(model);
            spawnedGeometryObjectViews.Add(view);
        }
    }
}

