﻿using Scripts.Core.Async;
using Scripts.Core.Types;

namespace Scripts
{
    public class EntryPoint: Singleton<EntryPoint>
    {
        private void Start()
        {
            ThreadTools.Initialize();
            App.Start();
        }
    }
}
