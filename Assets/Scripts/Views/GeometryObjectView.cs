﻿using Scripts.Models;
using UnityEngine;

namespace Scripts.Views
{
    [SelectionBase]
    public class GeometryObjectView : BaseView<GeometryObjectModel>
    {
        private MeshFilter[] meshFilters;

        private void Awake()
        {
            meshFilters = GetComponentsInChildren<MeshFilter>();
        }

        public override void OnInitialize()
        {
            base.OnInitialize();
            Model.OnColorChangedEvent += OnColorChanged;
        }

        private void OnDestroy()
        {
            Model.OnColorChangedEvent -= OnColorChanged;
        }

        private void OnColorChanged()
        {
            for (int i = 0; i < meshFilters.Length; i++)
            {
                Color[] colors = new Color[meshFilters[i].mesh.vertexCount];

                for (int j = 0; j < colors.Length; j++)
                {
                    colors[j] = Model.Color;
                }

                meshFilters[i].mesh.colors = colors;
            }
        }

        public void OnClick()
        {
            Model.OnClick();
        }
    }
}