﻿using UnityEngine;

namespace Scripts.Views
{
    public class BaseView<T> : MonoBehaviour
    {
        public T Model { get; private set; }

        public void Initialize(T model)
        {
            Model = model;
            OnInitialize();
        }

        public virtual void OnInitialize()
        {

        }
    }
}