﻿using System.IO;
using UnityEngine;

public class ScriptableObjectsService
{
    private const string ROOT_FOLDER = "Data/Settings";

    public T ReadScriptableObject<T>(string localPath) where T: ScriptableObject
    {
        string path = Path.Combine(ROOT_FOLDER, localPath);
        T scriptableObject = Resources.Load<T>(path);
        return scriptableObject;
    }
}
