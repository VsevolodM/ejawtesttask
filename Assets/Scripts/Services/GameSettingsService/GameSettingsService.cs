﻿using Scripts;
using Scripts.DataModel;

public class GameSettingsService
{
    private const string PATH_TO_GAME_SETTINGS = "General/GameSettingsData";

    private GameSettingsData gameSettingsData;

    public GameSettingsData GameSettingsData
    {
        get
        {
            if (gameSettingsData == null)
            {
                gameSettingsData = App.Services.ScriptableObjectsService.ReadScriptableObject<GameSettingsData>(PATH_TO_GAME_SETTINGS);
            }

            return gameSettingsData;
        }
    }
}
