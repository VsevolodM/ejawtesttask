﻿using Scripts.Models;

namespace Scripts.Services.DataProvider
{
    public class DataProviderService
    {
        public void ReadAllData()
        {
            ReadGeometryData();
        }

        public void ReadGeometryData()
        {
            GeometryDataModel geometryDataModel = App.Services.InternalStorage.GeometryDataModel.Read();
            App.Model.SetGeometryDataModel(geometryDataModel);
        }

        public void ReadGeometryBundles()
        {

        }
    }
}
