﻿using Scripts.Core.Async;
using Scripts.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class AssetBundlesService
{
    public void ReadAllBundlesFromDirectory<T>(string path, string[] names, Action<T[]> callback) where T : Component
    {
        string[] assetBundles = Directory.GetFiles(path, "*.asset");
        ThreadTools.Helper.StartCoroutine(ReadAllBundlesFromDirectoryCoroutine(assetBundles, names, callback));
    }

    private IEnumerator ReadAllBundlesFromDirectoryCoroutine<T>(string[] assetBundles, string[] names, Action<T[]> callback) where T : Component
    {
        List<T> objects = new List<T>();

        for (int i = 0; i < assetBundles.Length; i++)
        {
            yield return GetBundleContentCoroutine<T>(assetBundles[i], names, (loaded) => objects.AddRange(loaded));
        }

        callback.SafeInvoke(objects.ToArray());
    }

    public void GetBundleContent<T>(string path, string[] names, Action<T[]> callback) where T : Component
    {
        ThreadTools.Helper.StartCoroutine(GetBundleContentCoroutine(path, names, callback));
    }

    private IEnumerator GetBundleContentCoroutine<T>(string path, string[] names, Action<T[]> callback) where T : Component
    {
        var bundleLoadRequest = AssetBundle.LoadFromFileAsync(path);
        yield return bundleLoadRequest;

        var myLoadedAssetBundle = bundleLoadRequest.assetBundle;
        if (myLoadedAssetBundle == null)
        {
            Debug.Log("Failed to load AssetBundle!");
            yield break;
        }

        List<T> loadedAssets = new List<T>();
        string[] loadedNames = myLoadedAssetBundle.GetAllAssetNames();

        for (int i = 0; i < loadedNames.Length; i++)
        {
            string fileName = Path.GetFileNameWithoutExtension(loadedNames[i]);

            if (names.ContainsCaseInsensitive(fileName))
            {
                var assetLoadRequest = myLoadedAssetBundle.LoadAssetAsync<GameObject>(loadedNames[i]);
                yield return assetLoadRequest;
                loadedAssets.Add((assetLoadRequest.asset as GameObject).GetComponent<T>());
            }
        }

        callback.SafeInvoke(loadedAssets.ToArray());
        myLoadedAssetBundle.Unload(false);
    }
}
