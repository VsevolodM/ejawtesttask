﻿namespace Scripts.Services.AppStart
{
    public class AppStartService
    {
        public void Initialize()
        {
            App.Services.DataProvider.ReadAllData();
        }
    }
}
