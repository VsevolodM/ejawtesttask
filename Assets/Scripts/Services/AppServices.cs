﻿using Scripts.Core.Async;
using Scripts.Services.AppStart;
using Scripts.Services.DataProvider;
using Scripts.Services.InternalStorage;
using UnityEngine.SceneManagement;

namespace Scripts.Services
{
    public class AppServices
    {
        public AppStartService AppStart { get; }
        public InternalStorageService InternalStorage { get; }
        public DataProviderService DataProvider { get; }
        public AssetBundlesService AssetBundlesService { get; }
        public ScriptableObjectsService ScriptableObjectsService { get; }
        public GameSettingsService GameSettingsService { get; }

        public AppServices()
        {
            AppStart = new AppStartService();
            InternalStorage = new InternalStorageService();
            DataProvider = new DataProviderService();
            AssetBundlesService = new AssetBundlesService();
            ScriptableObjectsService = new ScriptableObjectsService();
            GameSettingsService = new GameSettingsService();

            App.ApplicationStartEvent += OnApplicationStart;
        }

        private void OnApplicationStart()
        {
            DataProvider.ReadAllData();
            SceneManager.LoadScene("SpawnScene", LoadSceneMode.Additive);
        }
    }
}
