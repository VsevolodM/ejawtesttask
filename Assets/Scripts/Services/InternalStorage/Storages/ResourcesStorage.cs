﻿using Scripts.Utils;
using UnityEngine;

namespace Scripts.Services.InternalStorage.Storages
{
    public class ResourcesStorage<T>
    {
        private const string PATH = "Data/Json/";
        private  string Key;

        public void SetKey(string newKey)
        {
            Key = PATH + newKey;
        }

        public T Read()
        {
            var json = GetJson();
            return SerializationUtils.Read<T>(json);
        }

        protected string GetJson()
        {
            var asset =  Resources.Load<TextAsset>(Key);
            if (asset == null)
            {
                Debug.LogErrorFormat("Can't read asset. Path: {0}", Key);
                return string.Empty;
            }

            return asset.text;
        }


    }
}
