﻿using Scripts.Services.InternalStorage.Storages;

namespace Scripts.Services.InternalStorage
{
    public class InternalStorage<T>
    {
        private readonly ResourcesStorage<T> resourcesStorage;

        public InternalStorage(string key)
        {
            resourcesStorage = new ResourcesStorage<T>();
            SetKey(key);
        }

        public InternalStorage(string playerPrefsKey, string resourcesKey)
        {
            resourcesStorage = new ResourcesStorage<T>();
            SetKeys(playerPrefsKey, resourcesKey);
        }

        public void SetKey(string key)
        {
            resourcesStorage.SetKey(key);
        }

        public void SetKeys(string playerPrefsKey, string resourcesKey)
        {
            resourcesStorage.SetKey(resourcesKey);
        }

        public T Read()
        {
            return resourcesStorage.Read();
        }
    }
}
