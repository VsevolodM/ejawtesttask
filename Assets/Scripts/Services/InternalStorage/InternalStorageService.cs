﻿using Scripts.Models;

namespace Scripts.Services.InternalStorage
{
    public class InternalStorageService
    {
        public InternalStorage<GeometryDataModel> GeometryDataModel { get; }

        public InternalStorageService()
        {
            GeometryDataModel = new InternalStorage<GeometryDataModel>("GeometryDataModel");
        }
    }
}
