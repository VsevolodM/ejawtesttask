﻿using Scripts.Extensions;
using System;
using System.Collections;
using Scripts.Core.Async;
using Scripts.DataModel;
using UnityEngine;

namespace Scripts.Models
{
    public class GeometryObjectModel
    {
        public event Action OnColorChangedEvent;

        public int ClicksAmount { get; private set; }

        public Color Color { get; private set; }
        private GeometryObjectData data;
        private WaitForSeconds autoRecolorDelay;
        private Coroutine waitTillAutoRecolorCoroutine;

        public GeometryObjectModel(GeometryObjectData data, float secondsTillAutoRecolor)
        {
            this.data = data;
            autoRecolorDelay = new WaitForSeconds(secondsTillAutoRecolor);
            StartRecolorCoroutine();
        }

        ~GeometryObjectModel()
        {
            StopRecolorCoroutine();
        }

        public void OnClick()
        {
            ClicksAmount++;

            Color newColor = data.GetColor(ClicksAmount);
            SetColor(newColor);
        }

        private IEnumerator WaitTillAutoRecolor()
        {
            yield return autoRecolorDelay;
            Color newRandomColor = ColorExtensions.GetRandomColor();
            SetColor(newRandomColor);
        }

        private void SetColor(Color color)
        {
            Color = color;
            StartRecolorCoroutine();
            OnColorChangedEvent.SafeInvoke();
        }

        private void StartRecolorCoroutine()
        {
            StopRecolorCoroutine();
            waitTillAutoRecolorCoroutine = ThreadTools.Helper.StartCoroutine(WaitTillAutoRecolor());
        }

        private void StopRecolorCoroutine()
        {
            if (waitTillAutoRecolorCoroutine != null)
            {
                ThreadTools.Helper.StopCoroutine(waitTillAutoRecolorCoroutine);
            }
        }
    }
}