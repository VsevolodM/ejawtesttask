﻿namespace Scripts.Models
{
    public class AppModel
    {
        public GeometryDataModel GeometryDataModel { get; private set; }

        public AppModel()
        {
            GeometryDataModel = new GeometryDataModel();
        }

        public void SetGeometryDataModel(GeometryDataModel geometryDataModel)
        {
            GeometryDataModel = geometryDataModel;
        }
    }
}
