﻿using Newtonsoft.Json;

namespace Scripts.Models
{ 
    [JsonObject(MemberSerialization.OptIn)]
    public class GeometryDataModel
    {
        [JsonProperty]
        public string[] GeometryItemsNames { get; private set; }
    }
}