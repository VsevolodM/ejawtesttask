﻿using Scripts;
using System.IO;
using Scripts.DataModel;
using Scripts.Models;

public static class GeometryObjectModelFactory
{
    private static string OBJECT_DATA_PATH = "ObjectData";
    private static float timeToAutoRecolor;

    static GeometryObjectModelFactory()
    {
        timeToAutoRecolor = App.Services.GameSettingsService.GameSettingsData.TimeToAutoRecolor;
    }

    public static GeometryObjectModel Create(string type)
    {
        string path = Path.Combine(OBJECT_DATA_PATH, type);
        GeometryObjectData data = App.Services.ScriptableObjectsService.ReadScriptableObject<GeometryObjectData>(path);
        GeometryObjectModel obj = new GeometryObjectModel(data, timeToAutoRecolor);
        return obj;
    }
}
