﻿using System;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;

namespace Scripts.Utils
{
    public static class SerializationUtils
    {
        public static string Serialize(object data)
        {
            string json = string.Empty;
            try
            {
                json = JsonConvert.SerializeObject(data);
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }

            return json;
        }

        public static T Read<T>(string json)
        {
            if (!string.IsNullOrEmpty(json))
            {
                try
                {
                    return JsonConvert.DeserializeObject<T>(json);
                }
                catch (Exception e)
                {
                    Debug.Log("json: "+ json);
                    Debug.LogError(e);
                    return default(T);
                }
            }

            return default(T);
        }

        public static bool TryDeserialize<T>(string json, ref T data)
        {
            if (!string.IsNullOrEmpty(json))
            {
                try
                {
                    data = JsonConvert.DeserializeObject<T>(json);
                    return true;
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                    return false;
                }
            }

            Debug.LogError("Can't read data. Json is empty");
            return false;
        }

        public static bool TryPatch(string json, object data)
        {
            if (!string.IsNullOrEmpty(json))
            {
                try
                {
                    var serializer = new JsonSerializer();
                    using (var reader = new StringReader(json))
                    {
                        serializer.Populate(reader, data);
                    }

                    return true;
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                    return false;
                }
            }

            Debug.LogError("Can't read data. Json is empty");
            return false;
        }
    }
}
