﻿using System;
using System.Collections.Generic;
using System.Linq;
using Random = UnityEngine.Random;

public static class CollectionExtensions
{
    public static T GetRandom<T>(this IEnumerable<T> enumerable)
    {
        int max = enumerable.Count();

        if (max == 0)
        {
            return default(T);
        }

        return enumerable.ElementAt(Random.Range(0, max));
    }

    public static bool ContainsCaseInsensitive(this IEnumerable<string> enumerable, string str)
    {
        int max = enumerable.Count();

        if (max == 0)
        {
            return false;
        }

        for (int i = 0; i < max; i++)
        {
            bool isEqual = String.Equals(enumerable.ElementAt(i), str, StringComparison.OrdinalIgnoreCase);

            if (isEqual)
            {
                return true;
            }
        }

        return false;
    }
}
