﻿using UnityEngine;

public static class GameObjectExtensions
{
    public static string GetUnclonedName(this GameObject gameObject)
    {
        return gameObject.name.Replace("(Clone)", string.Empty);
    }
}
