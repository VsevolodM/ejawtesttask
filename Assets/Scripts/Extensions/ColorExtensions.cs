﻿using UnityEngine;

public static class ColorExtensions
{
    public static Color GetRandomColor()
    {
        return new Color(Random.value, Random.value, Random.value, 1.0f);
    }
}
